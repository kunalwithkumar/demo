package com.wipro.SpringMapping.Service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.wipro.SpringMapping.Entity.User;
import com.wipro.SpringMapping.Entity.UserProfile;
import com.wipro.SpringMapping.Repository.UserProfileRepo;

@Service
public class UserProfileService {

	@Autowired
	private UserProfileRepo repo;

	@Autowired
	private UserService userService;

	public UserProfile adduserProfile(UserProfile profile) {

		UserProfile userProfile = new UserProfile();
		userProfile.setAge(profile.getAge());
		userProfile.setAddress(profile.getAddress());

		User user = userService.getUsers().get(0);

		userProfile.setUser(user);

		return repo.save(profile);
	}

	public List<UserProfile> getUsersProfile() {
		return repo.findAll();
	}
}
