package com.wipro.SpringMapping.Controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.wipro.SpringMapping.Entity.User;
import com.wipro.SpringMapping.Service.UserService;

@RestController
@RequestMapping("/user")
public class UserController {

	@Autowired
	private UserService service;

	@PostMapping("/add")
	public User addUser(@RequestBody User user) {
		return service.addUser(user);
	}

	@GetMapping("/get")
	public List<User> getUsers() {
		return service.getUsers();
	}

}
