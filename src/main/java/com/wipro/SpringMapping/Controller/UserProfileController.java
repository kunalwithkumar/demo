package com.wipro.SpringMapping.Controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.wipro.SpringMapping.Entity.UserProfile;
import com.wipro.SpringMapping.Service.UserProfileService;

@RestController
@RequestMapping("/profile")
public class UserProfileController {

	@Autowired
	private UserProfileService service;

	@PostMapping("/add")
	public UserProfile addUserProfile(@RequestBody UserProfile profile) {
		return service.adduserProfile(profile);
	}

	@GetMapping("/get")
	public List<UserProfile> getUsersProfile() {
		return service.getUsersProfile();
	}

}
